# SHEEPS KEEPERS

![](https://ideascdn.lego.com/media/generate/lego_ci/c4d4a8a6-ef61-40e5-8a67-8ed7371d6063/resize:1600:900/webp)

## Result

- See the [LEGO Idea page](https://ideas.lego.com/challenges/6811cf30-f944-4dfa-8714-9b38be6fbb52/application/5ed93b53-2695-41a1-8870-28c7a2afbea4)
- Play it on [Unity Play](https://play.unity.com/mg/lego/webgl-builds-19826)

![](https://ideascdn.lego.com/media/generate/lego_ci/91a41747-688a-4f2b-9455-5f2068373935/resize:1600:900/webp)
